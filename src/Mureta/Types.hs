{-# LANGUAGE DataKinds      #-}
{-# LANGUAGE DeriveGeneric  #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE TypeOperators  #-}

module Mureta.Types where

import           Control.Monad.Trans.Reader (ReaderT, runReaderT)
import           Data.Aeson                 (FromJSON, ToJSON, Value)
import           Data.IORef                 (IORef)
import qualified Data.Map                   as M
import           Data.UUID
import           GHC.Generics               (Generic)
import           Servant

newtype RelatorioReq = RelatorioReq
  { matriz :: String
  } deriving (Show, Generic)

instance FromJSON RelatorioReq

instance ToJSON RelatorioReq

data RelatorioResponse = RelatorioResponse
  { status_code :: Int
  , result      :: Value
  } deriving (Show, Generic)

instance FromJSON RelatorioResponse

instance ToJSON RelatorioResponse

type PostRelatorio
   = ReqBody '[ JSON] RelatorioReq :> Post '[ JSON] RelatorioResponse

type GetParametros
   = Capture "idrelatorio" UUID :> "parametros" :> Get '[ JSON] RelatorioResponse

data State = State
  { reports    :: IORef (M.Map UUID Value)
  , webhookUrl :: String
  , timeout    :: Int
  }

type AppM = ReaderT State Handler

type MuretaAPI = "relatorios" :> (PostRelatorio :<|> GetParametros)

api :: Proxy MuretaAPI
api = Proxy

nt :: State -> AppM a -> Handler a
nt s x = runReaderT x s
