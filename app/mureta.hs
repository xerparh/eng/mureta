{-# LANGUAGE DataKinds         #-}
{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE NamedFieldPuns    #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeOperators     #-}

module Main where

import           Mureta.Handlers
import           Mureta.Types

import           Data.IORef               (newIORef)
import qualified Data.Map                 as M
import           Network.Wai.Handler.Warp (run)
import           System.Environment       (getEnv)

main :: IO ()
main = do
  port <- read <$> getEnv "magoo_mureta_port"
  url <- getEnv "magoo_mureta_webhook_url"
  timeout <- (* 1000000) . read <$> getEnv "magoo_mureta_response_delay_in_s"
  initial <- newIORef M.empty
  run port $
    app $ State {reports = initial, webhookUrl = url, timeout = timeout}
