{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE QuasiQuotes         #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Main where

import           Control.Concurrent       (MVar, ThreadId, forkIO, killThread,
                                           newEmptyMVar, putMVar, readMVar,
                                           takeMVar, threadDelay)
import           Control.Concurrent.Async (race)
import qualified Control.Exception        as E
import           Control.Lens             (has, (^?))
import           Data.Aeson.Lens          (key, _Integer, _String)
import qualified Data.ByteString          as SBS
import           Data.ByteString.Lazy     (ByteString)
import           Data.IORef
import qualified Data.Map                 as M
import           Data.Maybe               (isJust)
import           Data.Text.Encoding       (encodeUtf8)
import           Data.UUID                (UUID, fromText, toASCIIBytes)
import           HttpBin.Server           (serve)
import           Mureta.Handlers
import           Mureta.Types
import           Network.HTTP.Types       (methodPost)
import           Network.Wai              (Application)
import           Network.Wai.Test         (SResponse, simpleBody)
import           Snap.Http.Server.Config  (setBind, setPort, setStartupHook,
                                           setVerbose)
import           System.Random
import           Test.Hspec.Wai
import           Test.Hspec.Wai.JSON
import           Test.Tasty
import           Test.Tasty.Hspec
import           Test.Tasty.HUnit

main :: IO ()
main = do
  results <- newEmptyMVar
  (tid, Just binding) <- startServer results
  webhookSpec <- webhookTest binding results
  specs <- tests binding
  flip E.finally (killThread tid) . defaultMain $
    testGroup "Mureta" [webhookSpec, specs]

testApp :: String -> IO Application
testApp endpoint = do
  initial <- newIORef M.empty
  pure $
    app
      State {reports = initial, webhookUrl = endpoint <> "/post", timeout = 100}

startServer :: MVar ByteString -> IO (ThreadId, Maybe String)
startServer results = do
  started <- newEmptyMVar
  let go n
        | n >= 100 = putMVar started Nothing
        | otherwise = do
          let port = 8000 + n
              startedUp p = putMVar started (Just ("http://0.0.0.0:" <> p))
              mkCfg =
                return . setBind ("0.0.0.0") . setPort port . setVerbose False .
                setStartupHook (const (startedUp (show port)))
          serve mkCfg results `E.catch` \(_ :: E.IOException) -> go (n + 1)
  tid <- forkIO $ go 0
  (,) tid <$> takeMVar started

tests :: String -> IO TestTree
tests endpoint =
  testSpec "Endpoints" $ with (testApp endpoint) $ do
    describe "POST /relatorios" $ do
      it "RG: should respond with 200" $
        postJson "/relatorios" "{\"matriz\": \"ocr_rg_rf\"}" `shouldRespondWith`
        200
      it "RG: should respond with reportId" $ do
        response <- postJson "/relatorios" "{\"matriz\": \"ocr_rg_rf\"}"
        liftIO $ do
          Just 200 @=? simpleBody response ^? key "status_code" . _Integer
          has (key "result") (simpleBody response) @? "should have result key"
          has (key "result" . key "numero") (simpleBody response) @?
            "should have result.numero key"
          let Just reportId =
                simpleBody response ^? key "result" . key "numero" . _String
          isJust (fromText reportId) @? "reportId should be an UUID"
      it "CNH: should respond with 200" $
        postJson "/relatorios" "{\"matriz\": \"ocr_cnh_rf\"}" `shouldRespondWith`
        200
      it "CNH: should respond with reportId" $ do
        response <- postJson "/relatorios" "{\"matriz\": \"ocr_cnh_rf\"}"
        liftIO $ do
          Just 200 @=? simpleBody response ^? key "status_code" . _Integer
          has (key "result") (simpleBody response) @? "should have result key"
          has (key "result" . key "numero") (simpleBody response) @?
            "should have result.numero key"
          let Just reportId =
                simpleBody response ^? key "result" . key "numero" . _String
          isJust (fromText reportId) @? "reportId should be an UUID"
    describe "GET /relatorios/:REPORTID/parametros" $ do
      it "RG: returns parameters for existent report" $ do
        response0 <- postJson "/relatorios" "{\"matriz\": \"ocr_rg_rf\"}"
        let Just reportId =
              simpleBody response0 ^? key "result" . key "numero" . _String
        response <- get ("/relatorios/" <> encodeUtf8 reportId <> "/parametros")
        pure response `shouldRespondWith` 200
        liftIO $
          [json|
           {
           "result": {
             "atualizado_em": "2018-09-14T16:15:35.123Z",
             "mensagem": "Válido.",
             "nome": "ocr_rg_rf",
             "numero": #{reportId},
             "parametros": {
               "cpf": "64501382627",
               "data_de_expedicao_rg": "10/09/2013",
               "data_de_nascimento": "06/12/1973",
               "digito_rg": "X",
               "estado_expedicao_rg": "SP",
               "nome": "FULANO DA SILVA",
               "nome_da_mae": "MARIA DA SILVA",
               "nome_do_pai": "JOAO DA SILVA",
               "orgao_emissor_rg": "SSP",
               "rg": "123456"
             },
             "resultado": "VALID",
             "status": "CONCLUIDO",
             "documento_ocr": {
               "numero": null,
               "filename_front": "idwall-ocr-cnhs.s3.amazonaws.com/rg_e2810ece-d0d0-43bb-932a-89bcb0be3dc1e7db08c5-2ca6-4e0c-9780-4ae33e478738_front.png",
               "filename_back": "idwall-ocr-cnhs.s3.amazonaws.com/rg_e2810ece-d0d0-43bb-932a-89bcb0be3dc1e7db08c5-2ca6-4e0c-9780-4ae33e478738_back.png",
               "status": "OK",
               "rg": "123456X",
               "data_de_expedicao": "10/09/2013",
               "nome": "FULANO DA SILVA",
               "nome_do_pai": "JOAO DA SILVA",
               "nome_da_mae": "MARIA DA SILVA",
               "naturalidade": "S. PAULO - SP",
               "data_de_nascimento": "06/12/1973",
               "doc_origem": "SÃO PAULO - SP",
               "cpf": "64501382627",
               "orgao_emissor": "SSP",
               "estado_emissao": "SP",
               "id_usuario": "704",
               "matriz": "ocr_rg_rf",
               "id_protocolo": "5744432",
               "filename_full": null,
               "backoffice_num": "052c063582e15adebd45b2ff150f6250",
               "mask": false
             }
           },
           "status_code": 200
         }
         |] @=?
          simpleBody response
      it "CNH: returns parameters for existent report" $ do
        response0 <- postJson "/relatorios" "{\"matriz\": \"ocr_cnh_rf\"}"
        let Just reportId =
              simpleBody response0 ^? key "result" . key "numero" . _String
        response <- get ("/relatorios/" <> encodeUtf8 reportId <> "/parametros")
        pure response `shouldRespondWith` 200
        liftIO $
          [json|
          {
            "result": {
              "atualizado_em": "2018-09-14T17:18:12.203Z",
              "mensagem": "Válido.",
              "nome": "ocr_cnh_rf",
              "numero": #{reportId},
              "parametros": {
                "cnh": "123456789",
                "cnh_data_da_primeira_habilitacao": "23/01/1992",
                "cnh_data_de_validade": "08/12/2022",
                "cnh_estado_expedicao": "SP",
                "cnh_numero_espelho": "987654321",
                "cnh_numero_renach": "SP000000000",
                "cpf": "64501382627",
                "data_de_nascimento": "06/12/1973",
                "estado_expedicao_cnh": "SP",
                "estado_expedicao_rg": "SP",
                "nome": "FULANO DA SILVA",
                "nome_da_mae": "MARIA DA SILVA",
                "nome_do_pai": "JOAO DA SILVA",
                "orgao_emissor_rg": "SSP",
                "rg": "123456",
                "seguranca_cnh": "00000000000"
              },
              "resultado": "VALID",
              "status": "CONCLUIDO",
              "documento_ocr": {
                "filename_front": null,
                "id_usuario": "704",
                "status": "OK",
                "nome": "FULANO DA SILVA",
                "rg": "123456",
                "orgao_emissor_rg": "SSP",
                "estado_emissao_rg": "SP",
                "cpf": "64501382627",
                "data_de_nascimento": "06/12/1973",
                "nome_do_pai": "JOAO DA SILVA",
                "nome_da_mae": "MARIA DA SILVA",
                "categoria": "B",
                "numero_registro": "00000000000",
                "validade": "08/12/2022",
                "data_primeira_habilitacao": "23/01/1992",
                "observacoes": "A",
                "data_de_emissao": "08/12/2017",
                "numero_renach": "SP000000000",
                "numero_espelho": "987654321",
                "permissao": "",
                "orgao_emissor": "DETRAN",
                "local_emissao": "SAO PAULO",
                "estado_emissao": "SP",
                "acc": "",
                "numero": "15607475fb008f6b0ba0673b840d3a93",
                "numero_seguranca": "00000000000",
                "filename_back": null,
                "matriz": "validaCnhDefault",
                "id_protocolo": "5745168",
                "foto_perfil": null,
                "rg_digito": "",
                "filename_full": "idwall-ocr-cnhs.s3.amazonaws.com/cnh_14f8a4d4-7819-405f-b267-ffc2ac1dc5ff3338eaf2-fcc4-4ab5-a602-6882f97c4ed5_full.jpeg",
                "mask": false
              }
            },
            "status_code": 200
          }
          |] @=?
          simpleBody response
      it "should return 404 if report id is inexistent" $ do
        uuid <- liftIO (randomIO :: IO UUID)
        get ("/relatorios/" <> toASCIIBytes uuid <> "/parametros") `shouldRespondWith`
          404
      it "should return 404 if report id is not an UUID" $
        get ("/relatorios/xxx/parametros") `shouldRespondWith`
        404

webhookTest :: String -> MVar ByteString -> IO TestTree
webhookTest endpoint results =
  testSpec "Webhook" $ with (testApp endpoint) $ do
    it "should send webhook after timeout" $ do
      response <- postJson "/relatorios" "{\"matriz\": \"ocr_rg_rf\"}"
      let Just reportId = simpleBody response ^? key "result" . key "numero"
      Right body <- liftIO $ race (threadDelay 2000000) (readMVar results)
      liftIO $ body @?=
        [json| {dados: {status: "CONCLUIDO", protocolo: #{reportId}}, tipo: "protocolo_status"} |]

postJson :: SBS.ByteString -> ByteString -> WaiSession SResponse
postJson path body =
  request methodPost path [("content-type", "application/json")] body
